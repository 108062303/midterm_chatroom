# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|20%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

# 作品網址：https://midterm-574a5.web.app/

## Website Detail Description
這是一個簡易聊天室，包含論壇及個人聊天室。
而且可以註冊帳號，相當方便。


# Components Description : 
1. Membership Mechanism : 可以在聊天室創辦帳號。設定好帳號密碼後，之後就可以登入及登出。
2. Host on your Firebase page : 我的聊天室有和firebase page連動，因此在我的firebase可以看見聊天室內所有的對話紀錄，且我有權利刪除大家的對話。
3. Database read/write : 登入帳號後，可以隨意read/write論壇上的留言，也可read/write個人聊天室的留言。
4. RWD : 當縮小或放大網頁時，聊天室的按鈕或對話框也會跟著縮小或放大。
5. Topic Key Functions : 可以像一般聊天室一樣正常運作。
6. Sign Up/In with Google or other third-party accounts : 我可以用google帳號登入及登出。
7. Add Chrome notification : 當我在聊天室時，若有其他人在論壇或個人聊天室留言時，chrome會通知說有新的留言。
8. Use CSS animation : 我的聊天室的左上角"Chatroom"會上下擺動，且右上角的麻里愛也會追著兩津勘及跑。
9. Deal with messages when sending html code : 當使用者輸入HTML語法時，網頁不會輸出HTML產生的物件，而是原封不動的輸出內文。

# Other Functions Description : 
1. 我的聊天室不只可以上傳文字，也有上傳圖片的功能。
2. 我的聊天室不只有個人聊天室，還有公共論壇，就算是沒辦帳號的使用者也可以閱讀論壇上的留言，並在論壇上留言。
3. 使用者可以在公共論壇及個人聊天室之間切換，想及時回去論壇或。
4. 剛進入聊天室時，會有使用者規範宣導，使用者必須遵守以上幾點規範後，才可以進入聊天室。
